The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.7.0] - 13-11-2020
### Added
- --no-fail-checksum CLI flag
### Changed
- Default behavior is now to fail the download if checksums don't match

## [0.6.1] - 08-11-2020
### Added
- Improve error handling
- Update Dockerfile

## [0.6.0] - 28-10-2020
### Added
- --input-file parameter

## [0.5.0] - 22-10-2020
### Added
- list-orders functionality
- download order functionality
- download scenes functionality
