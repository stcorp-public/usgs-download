FROM alpine:3.12.1 as builder

LABEL maintainer Robin Skahjem-Eriksen (skahjem-eriksen@stcorp.no)

RUN apk add --no-cache \
    cargo \
    openssl-dev

ADD . /tmp/build/

RUN cd /tmp/build \
    && cargo build --bins --release

FROM alpine:3.12.1
COPY --from=builder /tmp/build/target/release/usgs-download /usr/bin
RUN apk add --no-cache \
    libgcc \
    openssl

ENTRYPOINT ["/usr/bin/usgs-download"]
