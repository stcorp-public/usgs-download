# USGS ESPA order and download tool

A tool for ordering and downloading scenes from USGS ESPA.

List the complete orders for the authenticated user:
```
./usgs-download list-orders --status complete
```

Skip the `--status` flag to see all orders, including the purged ones.

Download an order:
```
./usgs-download order <orderID>
```

This will download the given order to the current directory. If some of the
target files are already in the directory, their checksums will be verified
to find out which ones can be skipped. A custom output directory can be set.

Download a set of scenes:
```
./usgs-download scenes --products sr,toa LC08_L1TP_197018_20181007_20181029_01_T1 LE07_L1TP_196019_20181024_20181119_01_T1
```

This will construct an order and proceed to wait until it can be downloaded,
and then proceed with downloading it to the given output directory. Pass the
`--order-only` flag to only order the scenes.

For full usage information, see:
```
$ ./usgs-download --help
```

![](st_logo.svg)

Made by: [S&T Norway](https://www.stcorp.no)
