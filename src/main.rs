use std::collections::HashMap;
use std::env::current_dir;
use std::fs::{create_dir_all, File};
use std::io::{BufRead, BufReader, Write};
use std::path::PathBuf;
use std::process::Command;
use std::str::from_utf8;

use anyhow::{anyhow, Result};
use rand::{thread_rng, Rng};
use reqwest::get;
use serde_json::to_string_pretty;
use structopt::StructOpt;
use usgs_espa_client::types::{
    AvailableProductItem, Credentials, FormatKind, OrderDefinition, OrderDetails, OrderStatusState,
    ProductOpt,
};
use usgs_espa_client::Client;

#[tokio::main]
async fn main() -> Result<()> {
    let config = Config::from_args();

    match config {
        Config::ListOrders(list_orders_config) => handle_list_orders(list_orders_config).await?,
        Config::Order(order_config) => handle_order(order_config).await?,
        Config::Scenes(scenes_config) => handle_scenes(scenes_config).await?,
    }

    Ok(())
}

/// Handle 'list-orders' subcommand
async fn handle_list_orders(config: ListOrdersConfig) -> Result<()> {
    let credentials = Credentials::from_env()?;
    let client = Client::new(credentials)?;
    let orders = client.list_orders().await?;

    match (&config.status, &config.details) {
        (None, false) => {
            let mut buffer = [0; 2];
            let output = orders.join(config.separator.encode_utf8(&mut buffer));
            write_output(output, config.output_file)?;
            return Ok(());
        }
        _ => {}
    }

    let mut detailed_orders: Vec<OrderDetails> = Vec::new();
    for order_id in orders {
        let detailed_order = client.get_order(&order_id).await?;
        detailed_orders.push(detailed_order);
    }

    let detailed_orders = match config.status {
        Some(status) => detailed_orders
            .into_iter()
            .filter(|order| status == order.status)
            .collect(),
        None => detailed_orders,
    };

    let output = match config.details {
        true => to_string_pretty(&detailed_orders)?,
        false => {
            let order_ids: Vec<String> = detailed_orders
                .into_iter()
                .map(|order| order.orderid)
                .collect();
            let mut buffer = [0; 2];
            order_ids.join(config.separator.encode_utf8(&mut buffer))
        }
    };

    write_output(output, config.output_file)?;

    Ok(())
}

fn write_output(output: String, filepath: Option<PathBuf>) -> Result<()> {
    match filepath {
        None => println!("{}", output),
        Some(path) => {
            let mut file = File::create(path)?;
            file.write_all(output.as_bytes())?;
        }
    }
    Ok(())
}

/// Handle 'order' subcommand
async fn handle_order(config: DownloadOrderConfig) -> Result<()> {
    let credentials = Credentials::from_env()?;
    let client = Client::new(credentials)?;
    download_order(
        config.order,
        &client,
        config.output_dir,
        config.no_fail_checksum,
    )
    .await?;
    Ok(())
}

/// Handle 'scenes' subcommand
async fn handle_scenes(config: DownloadScenesConfig) -> Result<()> {
    let credentials = Credentials::from_env()?;
    let client = Client::new(credentials)?;
    let (scenes, products, format, input_file) = (
        config.scenes,
        config.products,
        config.format,
        config.input_file,
    );

    let scenes_from_file = input_file.and_then(|filepath| {
        let lines = || -> Result<Vec<String>> {
            let file = File::open(filepath)?;
            BufReader::new(file).lines().map(|line| Ok(line?)).collect()
        };

        Some(lines())
    });

    let collections = match scenes_from_file {
        Some(val) => client.available_products(val?).await?,
        None => client.available_products(scenes).await?,
    };

    // Check if all scene IDs are valid
    match collections.get("not_implemented") {
        Some(scenes) => return Err(anyhow!("Found the following bad scene IDs: {}", scenes)),
        None => {}
    };

    let collections: HashMap<String, OrderDefinition> = collections
        .into_iter()
        .filter_map(|maybe_order_definition| match maybe_order_definition.1 {
            AvailableProductItem::OrderDefinition(definition) => {
                Some((maybe_order_definition.0, definition))
            }
            _ => None,
        })
        .collect();

    // Check if all collections support the required products
    for collection in collections.values() {
        for product in &products {
            if !collection.products.contains(&product) {
                return Err(anyhow!(
                    "Product: {} is not supported for use with collection: {}",
                    product,
                    collection
                ));
            }
        }
    }

    // Construct the post order request
    let mut order_request: HashMap<String, ProductOpt> = collections
        .into_iter()
        .map(|collection| {
            let collection_name = collection.0;
            let order_definition = ProductOpt::OrderDefinition(OrderDefinition {
                inputs: collection.1.inputs,
                products: products.clone(),
            });
            (collection_name, order_definition)
        })
        .collect();
    order_request.insert("format".to_owned(), ProductOpt::Format(format));

    println!("Order request: {}", to_string_pretty(&order_request)?);

    // Post the order
    let order_response = client.post_order(order_request).await?;

    if config.order_only {
        println!("Order response: \n{}", to_string_pretty(&order_response)?);
        return Ok(());
    };

    download_order(
        order_response.orderid,
        &client,
        config.output_dir,
        config.no_fail_checksum,
    )
    .await?;
    Ok(())
}

async fn download_order(
    order_id: String,
    client: &Client,
    output_dir: Option<PathBuf>,
    no_fail_checksum: bool,
) -> Result<()> {
    println!("Donwloading order: {}", order_id);

    let output_dir = match output_dir {
        Some(dir) => dir,
        None => current_dir()?,
    };

    create_dir_all(&output_dir)?;

    // Wait for completion
    let mut rng = thread_rng();
    loop {
        let timeout: u64 = rng.gen_range(420, 600);

        let order_status = client.order_status(&order_id).await?;

        match order_status.status {
            OrderStatusState::Purged => {
                return Err(anyhow!(
                    "Order {} can not be retrieved as it has been purged.",
                    order_id
                ))
            }
            OrderStatusState::Ordered => println!(
                "Order is still being processed. Will try again in {} seconds",
                timeout
            ),
            OrderStatusState::Complete => {
                println!("Order complete, proceed with download.");
                break;
            }
        }

        // Wait between 7 and 10 minutes, as the order preparation can take significant time,
        // and we shouldn't DDoS the server in the meantime.
        let duration = std::time::Duration::from_secs(timeout);
        std::thread::sleep(duration);
    }

    let order_items = client.item_status(&order_id, None).await?;
    let collection = match order_items.get(&order_id) {
        Some(col) => col,
        None => return Err(anyhow!("Empty order")),
    };

    for item_status in collection {
        let mut dl_response = get(&item_status.product_dload_url).await?;
        let expected_cksum = get(&item_status.cksum_download_url).await?.text().await?;
        let expected_cksum = expected_cksum
            .split_whitespace()
            .next()
            .unwrap_or("")
            .to_string();

        let fname = dl_response
            .url()
            .path_segments()
            .and_then(|segments| segments.last())
            .and_then(|name| if name.is_empty() { None } else { Some(name) })
            .unwrap_or("dataset.tif");
        let fname = output_dir.join(fname);

        println!(
            "Downloading scene: {} to: {}",
            item_status.name,
            fname.display()
        );

        if fname.exists() {
            println!("File exists, verifying checksum.");
            if verify_md5sum(&expected_cksum, &fname)? {
                println!("Continuing with the next scene.");
                continue;
            };
            println!("Downloading it again.")
        };

        let mut dest = File::create(&fname)?;

        while let Some(chunk) = dl_response.chunk().await? {
            dest.write(chunk.as_ref())?;
        }

        dest.flush()?;
        let md5sum_ok = verify_md5sum(&expected_cksum, &fname)?;

        if no_fail_checksum {
            continue;
        };

        if !md5sum_ok {
            return Err(anyhow!("Checksums don't match, aborting!"));
        };
    }

    println!("Order download complete!");

    Ok(())
}

fn verify_md5sum(expected_cksum: &str, fname: &PathBuf) -> Result<bool> {
    println!("Verifying checksum.");

    let md5sum = Command::new("md5sum")
        .arg(&fname)
        .output()
        .expect("Failed to execute md5sum");

    let cksum = from_utf8(&md5sum.stdout)?;
    let cksum = cksum.split_whitespace().next().unwrap_or("");

    if cksum == expected_cksum {
        println!("Checksums match.");
        return Ok(true);
    } else {
        println!("Checksums don't match for file: {}", fname.display());
        return Ok(false);
    }
}

/// A tool for ordering and downloading satellite data from USGS ESPA
#[derive(StructOpt, Debug)]
#[structopt(name = "usgs-download")]
enum Config {
    ListOrders(ListOrdersConfig),
    Order(DownloadOrderConfig),
    Scenes(DownloadScenesConfig),
}

/// Download the given order. Wait if the order is not yet complete.
///
/// To resume a previously interrupted order, run the command again with the same inputs.
#[derive(StructOpt, Debug)]
struct DownloadOrderConfig {
    /// Order to download
    order: String,
    /// Output directory
    #[structopt(short, long)]
    output_dir: Option<PathBuf>,
    /// Don't fail and stop the download on incorrect file checksum
    #[structopt(short, long)]
    no_fail_checksum: bool,
}

/// Order and download the given scenes
#[derive(StructOpt, Debug)]
struct DownloadScenesConfig {
    /// Scenes to download
    #[structopt(
        required_unless = "input-file",
        default_value_if("input-file", None, "")
    )]
    scenes: Vec<String>,
    /// Optional input file that contains scene IDs to download. One scene per line.
    #[structopt(short, long, conflicts_with = "scenes")]
    input_file: Option<PathBuf>,
    /// A comma separated list of data products to download. All scenes must support retrieving the given products.
    #[structopt(short, long, require_delimiter(true), default_value = "sr")]
    products: Vec<String>,
    /// Order only, don't wait for order completion
    #[structopt(long)]
    order_only: bool,
    /// Output directory
    #[structopt(short, long)]
    output_dir: Option<PathBuf>,
    /// Desired order file format
    #[structopt(short, long, possible_values(&["gtiff", "envi", "hdf-eos2", "netcdf"]), default_value = "gtiff")]
    format: FormatKind,
    /// Don't fail and stop the download on incorrect file checksum
    #[structopt(short, long)]
    no_fail_checksum: bool,
}

/// Retrieve order information of the currently logged in user
#[derive(StructOpt, Debug)]
struct ListOrdersConfig {
    /// Only return orders with the given status.
    #[structopt(short, long, possible_values(&["ordered", "complete", "purged"]))]
    status: Option<OrderStatusState>,
    /// Return detailed order information, instead of order IDs.
    #[structopt(short, long)]
    details: bool,
    /// Output separator
    #[structopt(long, default_value = "\n")]
    separator: char,
    /// Output filename to write, if not given, the output us written to stdout.
    #[structopt(short, long, parse(from_os_str))]
    output_file: Option<PathBuf>,
}
